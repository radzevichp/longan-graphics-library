package com.yulu.ean2d;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Ean2D {

    private static Logger log = LogManager.getLogger(Ean2D.class);

    public static final String TITLE = "C+ Ean2D Graphics library 0.1.0";

    ////eam2D Graphics library 2D 0.0.0

    public static final String VERSION = "0.1.0";
}
