package com.yulu.ean2d;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main
{
    public static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args)
    {
        /*
        log.trace("Exiting application.");
        log.debug("This is debug message.");    //  记录 debug 级别的信息
        log.info("This is info message.");      //  记录 info 级别的信息
        log.error("This is error message.");    //  记录 error 级别的信息

        log.info("------------------------------");
        log.info("Start information");
        log.info(Ean2D.TITLE);
        log.info(Ean2D.VERSION);
        log.info("------------------------------");
        */


        //log.info("Start main");
        init();
        final long mem = Runtime.getRuntime().maxMemory() >> 20;
        log.info("Max memory: {} " , (mem >= 1024 ? (mem >> 10) + " GB" : mem + " MB"));
    }



    //初始化
    private static void init()
    {
        log.info("Start init");
        MxPanel sd = new MxPanel();

        Frame frame = new Frame(Ean2D.TITLE);

        frame.setUndecorated(true);  // 禁用窗体装饰,加在前面就好了

        frame.setIconImage(Images.load("/icon.png"));



        frame.add(sd);
        frame.pack();
        //frame.setResizable(false);




        frame.setLocationRelativeTo(null);
        frame.addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e)
        {
            log.error(" Graphics library Screen End！");
            log.info("------------------------------");
            log.info("Start information");
            log.info(Ean2D.TITLE);
            log.info(Ean2D.VERSION);
            log.info("------------------------------");
            log.info("Screen Information");
            log.info("GAME_WIDTH: {}",MxPanel.GAME_WIDTH);
            log.info("GAME_HEIGHT: {}",MxPanel.GAME_HEIGHT);
            log.info("TICKS_PER_SECOND: {}",MxPanel.TICKS_PER_SECOND);
            log.info("------------------------------");

            final long mem = Runtime.getRuntime().maxMemory() >> 20;
            log.info("Max memory: {} " , (mem >= 1024 ? (mem >> 10) + " GB" : mem + " MB"));

            System.exit(0);
        }});
        frame.setVisible(true);
        sd.start();
    }




}