package com.yulu.ean2d;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;


public class MxPanel extends Panel implements Runnable
{

    private static Logger log = LogManager.getLogger(MxPanel.class);

    public static final int GAME_WIDTH = 1280;
    public static final int GAME_HEIGHT = 720;
    public static final int SCALE = 1;
    public static final int TICKS_PER_SECOND = 60;

    private Thread thread;
    private boolean running;
    private long lastTime;

    private BufferedImage offscreenImage = new BufferedImage(GAME_WIDTH, GAME_HEIGHT, BufferedImage.TYPE_INT_RGB);
    private Graphics offscreenGraphics = offscreenImage.getGraphics();

    private static BufferedImage logoScreen = Images.load("/title.png");

    public MxPanel()
    {
        /*
        log.info("------------------------------");
        log.info("Screen Information");
        log.info("GAME_WIDTH {}",GAME_WIDTH);
        log.info("GAME_HEIGHT {}",GAME_HEIGHT);
        log.info("TICKS_PER_SECOND {}",TICKS_PER_SECOND);
        log.info("------------------------------");
        */


        int w = GAME_WIDTH * SCALE;
        int h = GAME_HEIGHT * SCALE;
        setPreferredSize(new Dimension(w, h));
        setMaximumSize(new Dimension(w, h));
        setMinimumSize(new Dimension(w, h));
        setBackground(new Color(0,0,0));

        this.setFocusable(true);
    }

    public void paint(Graphics g)
    {
    }

    public void update(Graphics g)
    {
    }

    public synchronized void start()
    {
        running = true;
        lastTime = System.nanoTime();

        thread = new Thread(this,"");
        thread.start();

        requestFocus();
    }

    public synchronized void stop()
    {
        running = false;
    }

    public void run()
    {
        log.info("Start rendering");
        //Sounds.touch();
        long unprocessedTime = 0;
        long nsPerTick = 1000000000 / TICKS_PER_SECOND;
        int frames = 0;
        long lastFpsTime = System.currentTimeMillis();

        while (running)
        {
            long now = System.nanoTime();
            long passedTime = now - lastTime;
            if (passedTime > 0)
            {
                unprocessedTime += passedTime;
                while (unprocessedTime >= nsPerTick)
                {
                    unprocessedTime -= nsPerTick;

                }
                lastTime = now;
            }

            render(offscreenGraphics);
            frames++;

            if (System.currentTimeMillis()-lastFpsTime>1000)
            {
                lastFpsTime+=1000;
                log.info(frames+" fps");
                frames = 0;
            }

            Graphics g = getGraphics();
            g.drawImage(offscreenImage, 0, 0, GAME_WIDTH * SCALE, GAME_HEIGHT * SCALE, 0, 0, GAME_WIDTH, GAME_HEIGHT, null);
            g.dispose();

            try
            {
                Thread.sleep(2);
            }
            catch (InterruptedException e)
            {
            }
        }
    }


    public void render(Graphics g)
    {
        g.drawImage(logoScreen, 0, 0, null);
    }




}